<nav class="navbar is-transparent" role="navigation" aria-label="main navigation">
    <div class="container">
        <div class="navbar-brand">
                <a class="navbar-item" href="/">
                        <strong>Kazumi</strong>
                    </a>
            <span class="navbar-burger burger" data-target="navMenu">
                <span></span>
                <span></span>
                <span></span>
            </span>
        </div>
        <div class="navbar-menu" id="navMenu">
            <div class="navbar-start"></div>
            <div class="navbar-end">
                @if (Auth::guest())
                <a class="navbar-item" href="/">Register</a>
                <a class="navbar-item" href="{{ route('login') }}">Login</a>
                @else
                <a class="navbar-item" href="/">My Servers</a>
                <div class="navbar-item">




                    <a class="navbar-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout, {{ Auth::user()->username }}</a>
                </div>


                <form id="logout-form" action="{{ route('logout') }}" method="GET" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <form id="logs-form" action="/" method="GET" style="display: none;">
                    {{ csrf_field() }}
            </div>

        </div>
        @endif
    </div>
    </div>
    </div>
</nav>
@section ('js')
<script type="text/javascript">
    $(function () {
        $('.navbar-burger').click(function () {
            $(".navbar-burger").toggleClass("is-active")
            $(".navbar-menu").toggleClass("is-active")
        })
    })

</script>
